FROM gradle:7.5-jdk17-alpine AS build
WORKDIR /opt/recipehelper/service-discovery
COPY ./ ./
RUN gradle bootJar --no-daemon --stacktrace

FROM eclipse-temurin:17-jre-alpine
WORKDIR /opt/recipehelper/service-discovery
COPY --from=build /opt/recipehelper/service-discovery/build/libs/service-discovery.jar ./
EXPOSE 8761
ENTRYPOINT ["java", "-jar", "./service-discovery.jar"]
